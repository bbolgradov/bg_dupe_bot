EMOTICONS = {
    'thumbsup': '\U0001F44D'
}

# messages used when already posted today
TODAY = [
    'ей, ако още един път го пратите днес ще е за пръв път, да знаете...'
]

# messages used when it was posted yesterday
YESTERDAY = [
    'това всеки ден ли ще го пращате?'
]

# messages used when it was ever posted
DEFAULT = [
    'репост {}'.format(EMOTICONS["thumbsup"])
]

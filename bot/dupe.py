import hashlib
import logging
import re
from datetime import datetime, timedelta
from random import choice

from telegram.ext import Updater, RegexHandler, CommandHandler, MessageHandler, Filters

import bot
from bot import messages, responses
from bot.database import SQLite3


class DupeBot:
    def __init__(self):
        self.db = SQLite3()

    def handle_text(self, tbot, update):
        message = update.message or update.edited_message

        # check for a match in the regex map
        for regex, msg in responses.regex.items():
            if re.match(regex, message.text.lower()):
                tbot.send_message(chat_id=message.chat_id, text=msg)
                return

        sha1 = hashlib.sha1(str(message.text).encode('utf-8')).hexdigest()
        db_row = self.db.get_if_exists(message.chat_id, sha1)
        now = datetime.utcnow()

        if not db_row:
            logging.info('Saving new match: {} in chat {} at {}'.format(sha1, message.chat_id, now))
            self.db.save(message.chat_id, update.effective_user.name, sha1)
            return

        timestamp = datetime.strptime(db_row['timestamp'], '%Y-%m-%d %H:%M:%S')

        logging.info('Responding to: {} in chat {} at {}'.format(sha1, message.chat_id, timestamp))
        tbot.send_message(chat_id=message.chat_id, text=message.text)

    def handle_url(self, tbot, update):
        sha1 = hashlib.sha1(str(update.message.text).encode('utf-8')).hexdigest()
        db_row = self.db.get_if_exists(update.message.chat_id, sha1)
        now = datetime.utcnow()

        if not db_row:
            logging.info('Saving new match: {} in chat {} at {}'.format(sha1, update.message.chat_id, now))
            self.db.save(update.message.chat_id, update.effective_user.name, sha1)
            return

        timestamp = datetime.strptime(db_row['timestamp'], '%Y-%m-%d %H:%M:%S')

        # if last time this was posted was today
        if timestamp.date() == now.date():
            message = choice(messages.TODAY)

        # if yesterday
        elif timestamp.date() == (now - timedelta(days=1)).date():
            message = choice(messages.YESTERDAY)

        # if last month
        elif timestamp.date() >= (now - timedelta(days=30)).date():
            message = choice(messages.DEFAULT)

        # if older than a month, don't reply
        else:
            return

        logging.info('Responding to: {} in chat {} at {}'.format(sha1, update.message.chat_id, timestamp))
        tbot.send_message(chat_id=update.message.chat_id, text=message)

    def command_ping(self, tbot, update):
        tbot.send_message(chat_id=update.message.chat_id, text='в Русия съм, в Русия съм')

    def main(self):
        updater = Updater(token=bot.TOKEN)
        updater.dispatcher.add_handler(CommandHandler('ping', self.command_ping))
        updater.dispatcher.add_handler(RegexHandler(bot.MESSAGE_REGEX, self.handle_url))
        updater.dispatcher.add_handler(MessageHandler(Filters.text, self.handle_text, edited_updates=True))
        updater.start_polling(bootstrap_retries=-1)

import sqlite3


class SQLite3:
    def __init__(self):
        self.db = sqlite3.connect('dupe.sqlite3',
                                  check_same_thread=False)
        self.db.isolation_level = None
        self.db.row_factory = sqlite3.Row
        self.db.execute('''
            CREATE TABLE IF NOT EXISTS history (
              id INTEGER PRIMARY KEY,
              chat_id INT NOT NULL,
              user INT NOT NULL,
              message_hash TEXT NOT NULL,
              timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
              UNIQUE (chat_id, message_hash)
            )
        ''')
        self.cursor = self.db.cursor()

    def get_if_exists(self, chat_id: int, message_hash: str):
        self.cursor.execute('''
            SELECT * FROM history
            WHERE chat_id=? AND message_hash=?
            LIMIT 1
        ''', (chat_id, message_hash))
        result = self.cursor.fetchone()
        if result:
            self.cursor.execute('''
                UPDATE history
                SET timestamp = CURRENT_TIMESTAMP
                WHERE id=?
            ''', (result['id'],))
            self.cursor.fetchone()
        return result

    def save(self, chat_id: int, user: str, message_hash: str):
        self.cursor.execute('''
            INSERT INTO history (chat_id, user, message_hash)
            VALUES (?, ?, ?)
        ''', (chat_id, user, message_hash))

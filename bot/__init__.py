# this is the telegram bot token
TOKEN = None

# match the messages with this pattern
MESSAGE_REGEX = '^https?://[^.]+\.[^\s]+$'

try:
    from bot.settings import *
except ImportError:
    raise Exception('bot/settings.py is missing, create it with at least a TOKEN for the bot')

if not TOKEN:
    raise Exception('TOKEN is not defined - set it in bot/settings.py')
